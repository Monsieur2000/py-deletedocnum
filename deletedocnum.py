#!/bin/env python
# -*- coding: utf-8 -*-

# Script pour supprimer un document numérique du NAS et de la base Mango

from pymongo import MongoClient
import argparse
import getpass
import os
import sys
import glob
from dotenv import load_dotenv

# Arguments de la commande
parser = argparse.ArgumentParser()
parser.add_argument('-c', help="Cote à traiter", required=True)
parser.add_argument('-e', required=False, action="store_true", help="Etend le traitement aux sous-unités de la cote spécifiée avec -c")
args = parser.parse_args()

# erreur si -e est mis sans -c
if args.e and not args.c:
    exit("Une cote doit être spécifiée si -e est mentionné")

base_path = "/mnt/nas_archives"

# Connection à MangoDB
def connect_db():
    global collec
    global docs_num

    # On récupère les credentials s'ils existent sinon on les demandent
    # On peut les enregistrer dans un fichier .env
    load_dotenv()
    user = os.getenv("DB_USER")
    pwd = os.getenv("DB_PWD")

    if user is None:
        user = input("MongoDB user : ")

    if pwd is None:
        pwd = getpass.getpass(prompt="MongoDB password : ")

    # Création du client de la base Mongo
    mClient = MongoClient("mongodb://"+user+":"+pwd+"@srvfjme.unil.ch:27017/fjme-db")
    db = mClient['fjme-db']

    collec = db.uds
    docs_num = db.objets

# Récupération des infos sur les fichiers
query = {"cote" : args.c}
# si -e est indiqué, on modifie la requête
if args.e is True:
    query = {"cote" : {'$regex': f"^{args.c}"}}

#connexion
connect_db()

# test si y a des résultats
if docs_num.count_documents(query) <= 0:
    sys.exit("Aucun document ne porte cette cote dans la base de données")

# on boucle sur les résultats de la requête pour suppression
for doc in docs_num.find(query):
    print("Suppression des documents numériques...")
    os.remove(base_path + doc['localisation'])
    print(base_path + doc['localisation'], " supprimé")


# Détermination du type de document (image, audio, video)
print("Check si images intermédiaires dans mezzanine dans le cas des PDF...")
loc = docs_num.find_one(query)['localisation']
type_doc = loc.split('/')[3] # quatrième élément du chemin de localisation

# construction du chemin des images intermédiaires si ce sont des images pour PDF
if type_doc == 'image':
    cote_fond = args.c.split('-')[0]
    chemin = base_path + "/pieces/mezzanine/" + type_doc + "/_" + cote_fond
    if os.path.isdir(chemin):
        print("Suppression des images intermédiaires dans la mezzanine...")
        glob_path = f"{chemin}/{args.c}*"
        liste_fichiers = glob.glob(glob_path)
        for fichier in liste_fichiers:
            os.remove(fichier)
            print(fichier, " supprimé")
    else:
        print("Pas d'images intermédiaires dans mezzanine")

print("Suppression des entrées dans la base de données...")
d = docs_num.delete_many(query)
print(d.deleted_count, " entrées supprimées")
